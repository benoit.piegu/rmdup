/*  rmdup
 *  Author: Benoît Piégu <benoit.piegu@cnrs.fr>, 2023
 *
 * ---------------------------------------------------------------------------
 * rmdup is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
 *
 */
// use ahash::AHasher;
use anyhow::Context;
use clap::{arg, builder::styling, command, crate_authors, crate_description};
use needletail::parse_fastx_reader;
use rmdup::{setseq::CollisionHashSeq, transform_seq};
use std::{
    error::Error, fs::File, io::{self, BufReader, Read}, str
};

//type SeqHasher = AHasher;
type SeqHasher = fxhash::FxHasher;

const INPUT_DEFAULT_BUFSIZE: usize = 1024 * 16;
// const OUT_DEFAULT_BUFSIZE: usize = 1024 * 16;

/// the template for help
pub const HELP_TEMPLATE: &str = "\
{before-help}{name} (v{version})
{author}

{about-with-newline}
{usage-heading} {usage}

{all-args}{after-help}

";

fn main() -> core::result::Result<(), Box<dyn Error>> {
    let styles = styling::Styles::styled()
        .header(styling::AnsiColor::Green.on_default() | styling::Effects::BOLD)
        .usage(styling::AnsiColor::Green.on_default() | styling::Effects::BOLD)
        .literal(styling::AnsiColor::Blue.on_default() | styling::Effects::BOLD)
        .placeholder(styling::AnsiColor::Cyan.on_default());

    let app = command!()
        .about(crate_description!())
        .styles(styles)
        .author(crate_authors!(", "))
        .help_template(HELP_TEMPLATE)
        .arg_required_else_help(true)
        .arg(arg!(-d --debug ... "Sets the level of debugging")
            .required(false)
            .action(clap::ArgAction::Count),)
        .next_help_heading("Parameters")
        .arg(
            arg!(-i --ignore_case "Ignore case distinctions in sequences")
            .required(false)
            .action(clap::ArgAction::SetTrue),
        )
        .arg(
            arg!(-f --fasta_format "Force the use of fasta format in output")
            .required(false)
            .action(clap::ArgAction::SetTrue),
        )
        .arg(arg!(-r --with_revcomp ... "test the two strands")
            .required(false)
            .action(clap::ArgAction::SetTrue))
            .next_help_heading("Output")
        .arg(arg!(-o --out <OUT> "Output file name").required(false))
        .next_help_heading("Input")
        .arg(
            arg!(<FASTX> ... "Sets FASTA|FASTQ files to read. Use '-' to read STDIN")
            .value_parser(clap::value_parser!(String))
            .action(clap::ArgAction::Set)
            .num_args(clap::builder::ValueRange::new(1..)),
        );
    let arguments = app.get_matches();
    let _debug = arguments.get_count("debug");
    let fastx_files: Vec<&str> = arguments
        .get_many::<String>("FASTX")
        .unwrap()
        .map(|s| s.as_str())
        .collect();
    let with_ignore_case = arguments.get_flag("ignore_case");
    // let force_fasta = arguments.get_flag("fasta_format");
    let with_revcomp: bool = arguments.get_flag("with_revcomp");
    let mut hseq = CollisionHashSeq::<SeqHasher>::new(); //with_capacity(100000);
    let mut n_uniq_seq: usize = 0;
    let mut n_seq_tot: usize = 0;
    for (ibank, fastx_path) in fastx_files.iter().enumerate() {
        let mut n_seq: usize = 0;
        let (fastx_path, buff) = if fastx_path[..].eq("-") {
            let stdin = io::stdin();
            ("STDIN",
            Box::new(BufReader::with_capacity(INPUT_DEFAULT_BUFSIZE, stdin))
            as Box<dyn io::Read + Send>)
        } else {
            let file = File::open(fastx_path)
                .with_context(|| format!("couldn't open <{}>", &fastx_path))?;
            (*fastx_path,
                Box::new(BufReader::with_capacity(INPUT_DEFAULT_BUFSIZE, file))
                as Box<dyn Read + Send>)
        };
        let mut seqreader =
            parse_fastx_reader(buff).with_context(|| "fastx parse error".to_string())?;
        while let Some(seqrec) = seqreader.next() {
            let seqrec = seqrec.with_context(|| "Can't parse fastx stream")?;
            n_seq += 1;
            let seq = transform_seq(seqrec.seq(), with_ignore_case, with_revcomp);
            if hseq.is_new_seq(&seq) {
                n_uniq_seq += 1;
            }
        }
        eprintln!("# file {}: <{}>, n_seq={}", ibank + 1, fastx_path, n_seq);
        n_seq_tot += n_seq;
    }
    let mut n_collision = 0;
    hseq.hhashseq
        .iter()
        .filter(|(_k, setseq) | setseq.len() >1)
        .for_each(|(k, setseq)| {
            n_collision += 1;
            eprintln!("# k={}", k);
            setseq
                .iter()
                .enumerate()
                .for_each(|(i, seq)| eprintln!("  {}={}", i, unsafe { str::from_utf8_unchecked(seq) } ));
        });

    eprintln!("# n_seq_tot={} => n_uniq_seq={} ({:.2}), n_removed_seq={} n collision={}",
        n_seq_tot, n_uniq_seq, n_uniq_seq as f32 / n_seq_tot as f32, n_seq_tot-n_uniq_seq, n_collision);
    Ok(())
}
