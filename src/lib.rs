use std::{borrow::Cow, io};

use needletail::{errors::ParseError, parser::{write_fasta, write_fastq, LineEnding, SequenceRecord}};

pub mod setseq;

pub fn writeseq(seqrec: &SequenceRecord, out: & mut dyn io::Write, fasta: bool) -> Result<(), ParseError> {
    let format = seqrec.format();
    // write seq
    let res = if fasta || format == needletail::parser::Format::Fasta {
            write_fasta(seqrec.id(), &seqrec.seq(), out, LineEnding::Unix)
    } else {
        write_fastq(
            seqrec.id(),
            &seqrec.seq(),
            seqrec.qual(),
            out,
            LineEnding::Unix,
        )
    };
    res
}


#[inline(always)]
pub fn upppercase_seq<'a>(seq: Cow<'a, [u8]>) -> Cow<'a, [u8]> {
    Cow::from(seq.iter().map(|c| c.to_ascii_uppercase()).collect::<Vec<u8>>())
}

#[inline(always)]
pub fn canonical_seq<'a>(seq: Cow<'a, [u8]>) -> Cow<'a, [u8]> {
    let mut buf: Vec<u8> = Vec::with_capacity(seq.len());
    // enough just keeps our comparisons from happening after they need to
    let mut enough = false;
    let mut original_was_canonical = false;

    // loop through the kmer and its reverse complement simultaneously
    for (rn, n) in seq.iter().rev().map(|n| needletail::sequence::complement(*n)).zip(seq.iter()) {
        buf.push(rn);
        if !enough && (*n < rn) {
            original_was_canonical = true;
            break;
        } else if !enough && (rn < *n) {
            enough = true;
        }
        // unstated if branch: if rn == n, keep comparing
    }
    match (original_was_canonical, enough) {
        (true, true) => panic!("Bug: should never set original_was_canonical if enough == true"),
        (true, false) => seq.into(),
        (false, true) => buf.into(),
        // the sequences were completely equal, return the ref
        (false, false) => seq.into(),
    }
}

pub fn transform_seq<'a>(seq: Cow<'a, [u8]>, with_ignore_case: bool, with_revcomp: bool, ) -> Cow<'a, [u8]> {
    //let seq = seqrec.seq();
    let seq = if with_ignore_case {
        upppercase_seq(seq)
    } else {
        seq
    };
    let canonical_seq = if with_revcomp {
       canonical_seq(seq)
    } else {
        seq
    };
    canonical_seq
}