use std::{collections::{HashMap, HashSet}, hash::{BuildHasher, BuildHasherDefault, Hasher}};

use nohash_hasher::NoHashHasher;


pub struct SetSeq<H> {
    sethash: HashSet<u64, BuildHasherDefault<NoHashHasher<u64>>>,
    build_hasher: BuildHasherDefault<H>,
}

impl<H> SetSeq<H>
where
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    pub fn new() -> SetSeq<H> {
        SetSeq {
            sethash: HashSet::<u64, BuildHasherDefault<NoHashHasher<u64>>>::default(),
            build_hasher: <std::hash::BuildHasherDefault<H>>::default(),
        }
    }

    pub fn with_capacity(capacity: usize) -> SetSeq<H> {
        //  h: HashMap::<Vec<u8>, T, std::hash::BuildHasherDefault<H>>::with_capacity(capacity),
        let hasher = BuildHasherDefault::<NoHashHasher<u64>>::default();
        SetSeq {
            sethash: HashSet::<
                u64,
                BuildHasherDefault<NoHashHasher<u64>>
            >::with_capacity_and_hasher(capacity, hasher),
            build_hasher: <std::hash::BuildHasherDefault<H>>::default(),
        }
    }

    #[inline]
    pub fn is_new_seq(&mut self, seq: &[u8]) -> bool {
        let k = self.seq2hash(seq);
        // eprintln!("{} => {}", k,  unsafe { str::from_utf8_unchecked(seq) });
        self.sethash.insert(k)
    }

    #[inline]
    fn seq2hash(&self, seq: &[u8]) -> u64 {
        let mut hasher = self.build_hasher.build_hasher();
        hasher.write(seq);
        hasher.finish()
    }

    fn _len(& self) -> usize {
        self.sethash.len()
    }
}

impl<H> Default for SetSeq<H>
where
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    fn default() -> Self {
        Self::new()
    }
}

// type HSeq<H> = HashMap::<Vec<u8>, usize, std::hash::BuildHasherDefault<H>>;


type SetHashSeq<H> = HashSet<Vec<u8>, std::hash::BuildHasherDefault<H>>;

pub struct CollisionHashSeq<H> {
    /* hkseq: HashMap<
        u64, 
        HSeq<H>, 
        BuildHasherDefault<NoHashHasher<u64>>
    >,
    */
    pub hhashseq: HashMap<u64, SetHashSeq<H>, BuildHasherDefault<NoHashHasher<u64>>>,
    //pub hhashseq: HashMap<u64,  HashSet<Vec<u8>, std::hash::BuildHasherDefault<H>>, BuildHasherDefault<NoHashHasher<u64>>>,
    build_hasher: BuildHasherDefault<H>,
}

impl<H> CollisionHashSeq<H>
where
    H: Hasher + std::default::Default,
    BuildHasherDefault<H>: BuildHasher,
    //H: BuildHasher
{
    pub fn new() -> Self {
        CollisionHashSeq {
            //hkseq: HashMap::default(),
            hhashseq: HashMap::default(),
            // build_hasher: std::hash::BuildHasherDefault::<H>::default(),
            build_hasher: <std::hash::BuildHasherDefault<H>>::default(),
        }
    }
/*
    pub fn with_capacity(capacity: usize) -> Self {
        //  h: HashMap::<Vec<u8>, T, std::hash::BuildHasherDefault<H>>::with_capacity(capacity),
        CollisionHashSeq {
            hhashseq:  HashMap::<u64,  SetHashSeq<H>, BuildHasherDefault<NoHashHasher<u64>>>::with_capacity(capacity),
            build_hasher: <std::hash::BuildHasherDefault<H>>::default(),
        }
    }
*/
    fn seq2hash(&self, seq: &[u8]) -> u64 {
        let mut hasher = self.build_hasher.build_hasher();
        hasher.write(seq);
        hasher.finish()
    }

    pub fn is_new_seq(&mut self, seq: &[u8]) -> bool {
        let k = self.seq2hash(seq);
        // eprintln!("{} => {}", k,  unsafe { str::from_utf8_unchecked(seq) });
        let new = if let Some(hseq) = self.hhashseq.get_mut(&k) {
            // eprintln!("#   k={} exists", k);
            let exist = hseq.insert(seq.to_vec());
            exist
        } else {
            // eprintln!("#   k={} not exists", k);
            let mut hseq = SetHashSeq::<H>::default();
            // let mut hseq = HashSet::<Vec<u8>, BuildHasherDefault<H>>::default();
            hseq.insert(seq.to_vec());
            // eprintln!("#      {:?}", hseq);
            self.hhashseq.insert(k, hseq);
            true
        };
        new
    }
}
